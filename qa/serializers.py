from collections import OrderedDict

from django.urls import reverse
from django.http import request
from rest_framework import serializers

from .models import Question, Answer

class QuestionModelSerializer(serializers.ModelSerializer):
    # tags = serializers.SerializerMethodField()
    answers = serializers.SerializerMethodField()
    class Meta:
        model = Question
        fields = ['id', 'title', 'description', 'answers']
    
    # def get_tags(self, question):
    #     return [tag.name for tag in question.tags.all()]

    def get_answers(self, question):
        answers = []
        for answer in question.answers.all():
            answer_serializer = AnswerModelSerializer(answer,\
                context={'request':self.context['request']})
            answer_data = answer_serializer.data
            answer_data.update({'question':answer.question_id})
            answers.append(answer_data)
        return answers

    def save(self):
        user = self.context.get('request', None).user
        self.validated_data.update({'user': user})
        if self.instance is not None:
            self.instance = self.update(self.instance, self.validated_data)
        else:
            self.instance = self.create(self.validated_data)


class AnswerModelSerializer(serializers.ModelSerializer):

    question = serializers.HyperlinkedRelatedField(\
        view_name='qa:question-detail', read_only=True)

    class Meta:
        model = Answer
        fields = ['question', 'content', 'user', 'votes']
    
    def save(self):
        question_id = self.context.get('request', None)\
            .__dict__['parser_context']['kwargs']['pk']

        self.validated_data.update(\
            {'question': Question.objects.get(pk=question_id)})

        if self.instance is not None:
            self.instance = self.update(self.instance, self.validated_data)
        else:
            self.instance = self.create(self.validated_data)    
