from django.views import generic
from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from .models import Question, Answer
from .serializers import QuestionModelSerializer, AnswerModelSerializer


class GenericsQuestionList(generics.ListCreateAPIView):
    """
    List all post, or create a new post.
    """
    queryset = Question.objects.all()
    serializer_class = QuestionModelSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def post(self, request, *args, **kwargs):
        serializer = QuestionModelSerializer(data=request.data,\
                context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors,\
                status=status.HTTP_400_BAD_REQUEST)

class GenericsAnswerList(mixins.RetrieveModelMixin, generics.ListCreateAPIView):
    serializer_class = AnswerModelSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        question = Question.objects.get(pk=self.kwargs['pk'])
        return question.answers.all()

class GenericsQuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionModelSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def put(self, request, *args, **kwargs):
        question = Question.objects.get(pk=\
            request.parser_context['kwargs']['pk'])
        if question.user_id == request.user.id:
            serializer = QuestionModelSerializer(question, data=request.data,\
                context={'request': request})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors,\
                status=status.HTTP_400_BAD_REQUEST)
        return Response({"error":"Forbidden"},\
            status=status.HTTP_403_FORBIDDEN)

#-------------------------Non-Generic views below------------------------------#

class AnswerList(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    """
    List all post, or create a new post.
    """
    serializer_class = AnswerModelSerializer

    def get_queryset(self):
        question = Question.objects.get(pk=self.kwargs['pk'])
        return question.answers.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class QuestionList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    """
    List all post, or create a new post.
    """
    queryset = Question.objects.all()
    serializer_class = QuestionModelSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class QuestionDetail(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    generics.GenericAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionModelSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
