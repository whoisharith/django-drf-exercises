from django.urls import path

from .views import QuestionList, QuestionDetail, AnswerList,\
    GenericsQuestionList, GenericsQuestionDetail, GenericsAnswerList

app_name = 'qa'
urlpatterns = [
    path('', GenericsQuestionList.as_view(), name='question-list'),
    path('/<int:pk>', GenericsQuestionDetail.as_view(), name='question-detail'),
    path('/<int:pk>/answers', GenericsAnswerList.as_view(), name='answer-list'),
    # path('', QuestionList.as_view(), name='question-list'),
    # path('/<int:pk>', QuestionDetail.as_view(), name='question-detail'),
    # path('/<int:pk>/answers', AnswerList.as_view(), name='answer-list'),
]